import { v4 } from 'uuid';

export interface ITest {
  id: string,
  a: string,
  b: string
}

export function create(a: string, b: string): ITest {
  return {
    id: v4(),
    a: a,
    b: b
  };
}

export function toString(test: ITest): string {
  return test.id + ' --> ' + test.a + ' ' + test.b;
}

export function newFunction(): string {
  return 'This is from version 1.0.1';
}

export function newerFunction():string {
  return 'This is from version 1.1.0';
}

export function newestFunction():string {
  return 'This is from version 1.1.1';
}
