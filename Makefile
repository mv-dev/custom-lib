install:
	docker-compose run --rm install
build: install
	docker-compose run --rm build

dev:
	docker-compose up --remove-orphans dev
stop:
	docker-compose stop
down:
	docker-compose down

sh:
	docker-compose exec --user=root dev /bin/bash
	